# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-08-25 11:57:56
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-08-26 16:32:58
import numpy as np
import pandas as pd
from sklearn import cross_validation as cv

def Read_and_Divide(filename):
	header  = ['user_id', 'item_id','rating','timestamp']
	data = pd.read_csv(filename, sep = '::', names = header, engine = 'python')

	"""
	由用户和物品的id都是从1开始的连续整数
	所以读取user_id和item_id的最大值为对应的个数
	"""
	num_users = pd.Series.max(data.user_id)
	num_items = pd.Series.max(data.item_id)


	print "用户总数为："+str(num_users)
	print "物品总数为："+str(num_items)

	"""
	#采用这种方法读出来的物品个数为3706，代表有3706个物品被评过分
	#实际上应该是3952，代表实际上物品总数是3952
	#原因有可能是有的物品没有被评过分，但依旧收录在物品列表中
	self.num_users = df.user_id.unique().shape[0]#统计用户数
	self.num_items = df.item_id.unique().shape[0]#统计物品数
	"""
	train_data,test_data = cv.train_test_split(data,test_size = 0.25)
	train_data_matrix = np.zeros((num_users,num_items))

	for line in train_data.itertuples():
		train_data_matrix[line[1]-1, line[2]-1] = float(line[3])#这里构建的矩阵中的值是1-5的评分

	test_data_matrix = np.zeros((num_users,num_items))
	for line in test_data.itertuples():
		test_data_matrix[line[1]-1, line[2]-1] = float(line[3])#这里构建的矩阵中的值是1-5的评分

	return (train_data_matrix,test_data_matrix)

def confidence(matrix):
	num_users = matrix.shape[0]
	num_items = matrix.shape[1]
	total = matrix.sum()
	alpha = float(num_users*num_items/total)
	train_confidence_matrix = matrix*alpha
	return train_confidence_matrix
