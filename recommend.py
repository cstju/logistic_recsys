# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-08-25 20:44:56
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-08-26 15:50:28

import numpy as np
#import pandas as pd
#import scipy.sparse as sparse
#import time
#import pdb
#from scipy.sparse.linalg import spsolve
#from sklearn import cross_validation as cv
#from sklearn.metrics.pairwise import pairwise_distances
from sklearn.metrics import mean_squared_error
from math import sqrt
from operator import itemgetter

class Evaluation():
	"""docstring for Recommend"""
	def __init__(self, users_vectors,items_vectors,users_biases=None,items_biases=None):
		self.users_vectors = users_vectors
		self.items_vectors = items_vectors
		self.users_biases = users_biases
		self.items_biases = items_biases
		"""
		把计算预测结果的部分加到这个程序中，新建一个函数
		"""

	def rmse(self,ground_truth):#计算预测矩阵和真实矩阵之间的rmse
		self.prediction = self.predict()
		prediction = self.prediction[ground_truth.nonzero()].flatten()#将2维预测矩阵转化为1维数组
		ground_truth = ground_truth[ground_truth.nonzero()].flatten()
		return sqrt(mean_squared_error(prediction,ground_truth))

	def average_rank(self,ground_truth):#计算预测矩阵的平均排名
		self.num_users = self.prediction.shape[0]
		prediction_rank = np.zeros((self.num_users,1))
		for userid in range(self.num_users):
			rankid = 0.0
			prediction_useri = self.prediction[userid]#得到用户userid的预测值
			ground_truth_useri = ground_truth[userid]#用户userid的真实评分
			if np.sum(ground_truth_useri) == 0:
				continue
			prediction_useri_dec = {}
			for itemid in range(len(ground_truth_useri)):
				if ground_truth_useri[itemid] != 0:
					prediction_useri_dec.setdefault(itemid,0)
					prediction_useri_dec[itemid] = prediction_useri[itemid]#得到用户真实评分中的非0元素所对应的预测值和商品号

			rank_num = 10 if len(prediction_useri_dec)>10 else len(prediction_useri_dec)
			for idd,pui in sorted(prediction_useri_dec.iteritems(),key = itemgetter(1),reverse=True)[:rank_num]:#将预测分数中非0元素从大到小排序个(取前rank_num个)
			#rankid/rank_num 是得到相对排名，取值在0-1
				prediction_rank[userid][0] += (rankid/rank_num) * ground_truth_useri[idd]
				rankid += 1.0
			prediction_rank[userid][0] = prediction_rank[userid][0]/np.sum(ground_truth_useri)

		return np.sum(prediction_rank)/self.num_users
	def predict(self):
		prediction = self.users_vectors.dot(self.items_vectors.T)+self.users_biases+self.items_biases.T
		return prediction