# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-08-25 11:58:19
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-08-26 16:30:39
import readdata
from logistic import LogisticMF
import plot
from recommend import Evaluation
import pdb
def getData():
	pass

def main():
	filename = "/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 1M Dataset/ml-1m/ratings.dat"

	print '...........读取并分割数据............'
	train_data_matrix,test_data_matrix = readdata.Read_and_Divide(filename)
	print '.........读取并分割数据完成...........'
	"""
	print '...........增加信任度因子...........'
	train_confidence_data_matrix = readdata.confidence(train_data_matrix)#注意，这里直接对1-5分的评分增加信任度，而论文中是对
	print '...........增加信任度因子完成...........'
	"""
	#LogisticMF(counts, num_factors=10, reg_param=0.6, gamma=1.0,iterations=30)
	#将训练矩阵和参数输入到函数logisticMF中得到用户特征矩阵，商品特征矩阵，用户偏置向量，商品偏置向量

	#特征个数
	num_factors=10
	#正则化系数
	reg_param=0.6
	#特征迭代更新系数
	gamma=1.0
	#迭代次数
	iterations=30
	logisticmf = LogisticMF(train_data_matrix,num_factors,reg_param,gamma,iterations)

	print '.................开始计算特征向量...................'
	users_vectors,items_vectors,users_biases,items_biases=logisticmf.train_model()

	print '.................计算特征向量完成...................'
	eva = Evaluation(users_vectors,items_vectors,users_biases,items_biases)

	print '.................开始算法检验...................'
	print '......特征个数= %i ,正则化系数= %f ,特征迭代更新系数= %f ,迭代次数= %i ......' %(num_factors,reg_param,gamma,iterations)

	print '.................开始计算RMSE...................'
	rmse = eva.rmse(test_data_matrix)
	print '.................RMSE计算完成,RMSE=%f...................' %rmse

	print '.................开始计算平均排名...................'
	average_rank = eva.average_rank(test_data_matrix)
	print '.................RMSE计算完成,average_rank=%f...................' %average_rank




if __name__=='__main__':
	main()